import React, { useEffect, useState, memo } from 'react';
import { isEmpty } from 'lodash';
import moment from 'moment';
import {
  Form,
  Row,
  Col,
  Select,
  Card,
  DatePicker,
  Button,
  Checkbox,
  message,
} from 'antd';

import '../../assets/less/search-bar.less';

const layout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 24 },
};

interface Props {
  onSearch: Function;
  onChangePlace: Function;
  originPlaces: any;
  destinationPlaces: any;
  currencies: any;
  countries: any;
  loading: boolean;
  loadingOrigin: boolean;
  loadingDestination: boolean;
}

const SearchBar: React.FC<Props> = memo(({
  onSearch,
  onChangePlace,
  originPlaces,
  destinationPlaces,
  currencies,
  countries,
  loadingOrigin,
  loadingDestination,
}) => {
  const { language } = navigator;
  const [onlyOrigin, setOnly] = useState(false);
  const [form] = Form.useForm();

  useEffect(() => {
    form.resetFields();
  }, [form]);

  const submit = (fields: any) => {
    onSearch(fields);
  };

  const changeCountry = (target: string, field: string) => {
    const { currency } = form.getFieldsValue();
    if (!currency) {
      message.error('Seleccione la moneda');
      form.resetFields([`${target}country`]);
      return;
    }
    form.resetFields([field]);
  };

  const changePlace = (value: string, target: string) => {
    const { currency, origincountry, destinationcountry } = form.getFieldsValue();

    if ((target === 'origin' && !origincountry) || (target === 'destination' && !destinationcountry)) {
      message.error('Seleccion el pais');
      form.resetFields([`${target}place`]);
      return;
    }

    if (value) {
      const payload = {
        currency,
        country: target === 'origin' ? origincountry : destinationcountry,
        query: value,
        locale: language,
      };
      onChangePlace(payload, target);
    }
  };

  const changeCheckbox = (e: any) => {
    setOnly(e.target.checked);
  };

  const disabledOriginDate = (current: any) => {
    return current && current.isBefore(moment());
  }

  const disabledDestinaitonDate = (current: any) => {
    const originDate = moment(form.getFieldValue('outboundpartialdate')).add(1, 'day');
    return current && current.isSameOrBefore(originDate);
  }

  const changeOriginDate = (date: any) => {
    const originDate = moment(form.getFieldValue('outboundpartialdate')).add(1, 'day');
    form.setFieldsValue({ inboundpartialdate: originDate });
  };

  return (
    <Form form={form} {...layout} onFinish={submit}>
      <Card
        bordered={false}
        className="card-search-flight"
        bodyStyle={{ padding: 0 }}
        title={<h2>Buscar vuelos</h2>}
        extra={[
          <Form.Item
            key="0"
            name="currency"
            rules={[{ required: true, message: <span/> }]}
          >
            <Select
              showSearch
              size="large"
              placeholder="Moneda"
              style={{ width: 150 }}
              disabled={isEmpty(currencies)}
            >
              {!isEmpty(currencies) && currencies.map((c: any) => <Select.Option key={c.Code} value={c.Code}>{`${c.Code} (${c.Symbol})`}</Select.Option>)}
            </Select>
          </Form.Item>
        ]}
        actions={[
          <Button
            type="primary"
            key="1"
            size="large"
            block
            disabled={loadingOrigin || loadingDestination}
            onClick={() => form.submit()}
          >
            Buscar
          </Button>,
        ]}
      >
        <Row gutter={8}>
          <Col className="mb-10" md={12} sm={24} xs={24}>
            <Card
              title="Origen"
              bodyStyle={{ padding: 12 }}
              extra={[
                <Checkbox key="1" onChange={changeCheckbox}><strong>Solo ida</strong></Checkbox>
              ]}
            >
              <Form.Item
                label="Pais"
                name="origincountry"
                rules={[{ required: true, message: 'Campo requerido' }]}
              >
                <Select
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input: any, option: any) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  size="large"
                  placeholder="Pais"
                  disabled={isEmpty(countries)}
                  onChange={() => changeCountry('origin', 'originplace')}
                >
                  {countries?.map((c: any) => <Select.Option key={c.Code} value={c.Code}>{`${c.Name} (${c.Code})`}</Select.Option>)}
                </Select>
              </Form.Item>
              <Form.Item
                label="Lugar"
                name="originplace"
                rules={[{ required: true, message: 'Campo requerido' }]}
              >
                <Select
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input: any, option: any) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  size="large"
                  placeholder="Lugar"
                  loading={loadingOrigin}
                  onSearch={(value: string) => changePlace(value, 'origin')}
                >
                  {originPlaces?.map((p: any) => <Select.Option key={p.PlaceId} value={p.PlaceId}>{`${p.PlaceName} (${p.PlaceId})`}</Select.Option>)}
                </Select>
              </Form.Item>
            </Card>
          </Col>
          <Col className="mb-10" md={12} sm={24} xs={24}>
            <Card title="Destino" bodyStyle={{ padding: 12 }}>
              <Form.Item
                label="Pais"
                name="destinationcountry"
                rules={[{ required: true, message: 'Campo requerido' }]}
              >
                <Select
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input: any, option: any) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  size="large"
                  placeholder="Pais"
                  disabled={isEmpty(countries) || onlyOrigin}
                  onChange={() => changeCountry('destination', 'destinationplace')}
                >
                  {countries?.map((c: any) => <Select.Option key={c.Code} value={c.Code}>{`${c.Name} (${c.Code})`}</Select.Option>)}
                </Select>
              </Form.Item>
              <Form.Item
                label="Lugar"
                name="destinationplace"
                rules={[{ required: true, message: 'Campo requerido' }]}
              >
                <Select
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input: any, option: any) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  size="large"
                  placeholder="Lugar"
                  disabled={onlyOrigin}
                  loading={loadingDestination}
                  onSearch={(value: string) => changePlace(value, 'destination')}
                >
                  {destinationPlaces?.map((p: any) => <Select.Option key={p.PlaceId} value={p.PlaceId}>{`${p.PlaceName} (${p.PlaceId})`}</Select.Option>)}
                </Select>
              </Form.Item>
            </Card>
          </Col>
        </Row>
        <Row gutter={8}>
          <Col className="mb-10" md={24} sm={24} xs={24}>
            <Card title="Fechas" bodyStyle={{ padding: 12 }}>
              <Row gutter={8}>
                <Col md={12} sm={24} xs={24}>
                  <Form.Item
                    label="Fecha de partida"
                    name="outboundpartialdate"
                    rules={[{ required: true, message: 'Campo requerido' }]}
                  >
                    <DatePicker
                      size="large"
                      placeholder="Fecha de partida"
                      style={{ width: '100%' }}
                      format="DD MMMM YYYY"
                      onChange={changeOriginDate}
                      disabledDate={disabledOriginDate}
                    />
                  </Form.Item>
                </Col>
                <Col md={12} sm={24} xs={24}>
                  <Form.Item
                    label="Fecha de regreso"
                    name="inboundpartialdate"
                  >
                    <DatePicker
                      size="large"
                      placeholder="Fecha de regreso"
                      style={{ width: '100%' }}
                      format="DD MMMM YYYY"
                      disabled={onlyOrigin}
                      disabledDate={disabledDestinaitonDate}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Card>
    </Form>
  );
});

export default SearchBar;