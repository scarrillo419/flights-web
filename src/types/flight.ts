import { Currency } from '../models/currency';
import { Place } from '../models/place';
import { Country } from '../models/country';
import { Flight } from '../models/flight';

export enum FlightActionTypes {
  GET_ALL = '@@flight/GET_ALL',
  SET_ALL = '@@flight/SET_ALL',
  SET_LOADING = '@@flight/SET_LOADING',
  SET_ORIGIN_LOADING = '@@flight/SET_ORIGIN_LOADING',
  SET_DESTINATION_LOADING = '@@flight/SET_DESTINATION_LOADING',
  ERROR_REQUEST = '@@flight/ERROR_REQUEST',
  GET_CURRENCIES = '@@flight/GET_CURRENCIES',
  SET_CURRENCIES = '@@flight/SET_CURRENCIES',
  GET_PLACES = '@@flight/GET_PLACES',
  SET_ORIGIN_PLACES = '@@flight/SET_ORIGIN_PLACES',
  SET_DESTINATION_PLACES = '@@flight/SET_DESTINATION_PLACES',
  GET_COUNTRIES = '@@flight/GET_COUNTRIES',
  SET_COUNTRIES = '@@flight/SET_COUNTRIES',
}

export interface FlightState {
  readonly listFlights: Flight;
  readonly listCurrencies: Currency[];
  readonly listOriginPlaces: Place[];
  readonly listDestinationPlaces: Place[];
  readonly listCountries: Country[];
  readonly loading: boolean;
  readonly loadingOrigin: boolean;
  readonly loadingDestination: boolean;
  error: any;
}
