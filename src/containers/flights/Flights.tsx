import React, { useContext, useEffect, useCallback, memo } from 'react';
import {
  Spin,
} from 'antd';
import { useHistory } from 'react-router-dom';

import { prepareData } from '../../utils';
import SearchBar from '../../components/flights/SearchBar';

import { FlightContext } from '../../contexts/flight';
import { getFlights, getPlaces, getCountries, getCurrencies } from '../../services/flight';
import { errorRequest } from '../../actions/flight';

const Flights: React.FC = memo(() => {
  const history = useHistory();
  const { state, dispatch } = useContext(FlightContext);
  const {
    loading,
    loadingOrigin,
    loadingDestination,
    listOriginPlaces,
    listDestinationPlaces,
    listCurrencies,
    listCountries,
  } = state;

  useEffect(() => {
    document.title = 'Buscar vuelos';
    getCountries(dispatch);
    getCurrencies(dispatch);
  }, [dispatch]);

  const fetchFlights = useCallback((fields: any) => {
    const payload = prepareData(fields);
    getFlights(dispatch, payload)
      .then(() => history.push('/results'))
      .catch((e) => dispatch(errorRequest(e)));
  }, [dispatch, history]);

  const fetchPlaces = useCallback((payload, target) => {
    getPlaces(dispatch, { payload, target });
  }, [dispatch]);

  return (
    <Spin spinning={loading}>
      <SearchBar
        onSearch={fetchFlights}
        onChangePlace={fetchPlaces}
        originPlaces={listOriginPlaces}
        destinationPlaces={listDestinationPlaces}
        currencies={listCurrencies}
        countries={listCountries}
        loading={loading}
        loadingOrigin={loadingOrigin}
        loadingDestination={loadingDestination}
      />
    </Spin>
  );
});

export default Flights;