import React from 'react';
import { RouteProps, Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import {
  DingdingOutlined,
} from '@ant-design/icons';

import '../../assets/less/layout.less';

const { Header, Sider, Content, Footer } = Layout;

interface Props extends RouteProps {
  component: any;
}

export default (props: Props) => {
  const {
    component: Component,
  } = props;

  return (
    <Layout>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
      >
        <div className="logo" />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['flights-admin']}>
          <Menu.Item key="flights-admin" >
            <DingdingOutlined />
            <Link to="/flights">Vuelos</Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
        <Content style={{ margin: '24px 16px 0' }}>
          <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            <Component />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Flights ©2020 Desarrallado por Ing. Sergio Carrillo</Footer>
      </Layout>
    </Layout>
  );
}