import React, { useEffect, useContext } from 'react';
import {
  Descriptions,
  PageHeader,
  Divider,
  Card,
} from 'antd';
import {
  LeftOutlined,
} from '@ant-design/icons';
import moment from 'moment';
import { useHistory } from 'react-router-dom';

import { FlightContext } from '../../contexts/flight';

const Results: React.FC = () => {
  const history = useHistory();
  const { state } = useContext(FlightContext);
  const { listFlights } = state;

  useEffect(() => {
    document.title = `Resultados (${listFlights.Quotes.length})`;
  });

  const getPlace = (placeId: number) => {
    const { Places } = listFlights;
    const place: any = Places.find(p => p.PlaceId === placeId);
    return place.Name ?? '';
  }

  const getCarrier = (carrierIds: number[]) => {
    const { Carriers } = listFlights;
    return Carriers
      .filter(c => carrierIds.some(ci => ci === c.CarrierId))
      .map(c => c.Name)
      .join(', ');
  };

  return (
    <PageHeader
      title={`Resultados (${listFlights.Quotes.length})`}
      onBack={() => history.goBack()}
      backIcon={<LeftOutlined />}
    >
      <Card>
        {listFlights.Quotes?.map(q => (
          <div key={q.QuoteId}>
            <Descriptions title={`Precio: ${listFlights.Currencies[0].Symbol} ${q.MinPrice}`}>
              <Descriptions.Item label="Fecha de partida">{moment(q.OutboundLeg.DepartureDate).format('DD MMMM YYYY')}</Descriptions.Item>
              <Descriptions.Item label="Origen">{getPlace(q.OutboundLeg.OriginId)}</Descriptions.Item>
              <Descriptions.Item label="Destino">{getPlace(q.OutboundLeg.DestinationId)}</Descriptions.Item>
              <Descriptions.Item label="Aerolínea">{getCarrier(q.OutboundLeg.CarrierIds)}</Descriptions.Item>
            </Descriptions>
            <Divider />
          </div>
        ))}
      </Card>
    </PageHeader>
  );
}

export default Results;