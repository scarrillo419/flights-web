import { FlightActionTypes } from '../types/flight';

export const setLoading = (payload?: any) => ({ type: FlightActionTypes.SET_LOADING, payload });
export const setOriginLoading = (payload?: any) => ({ type: FlightActionTypes.SET_ORIGIN_LOADING, payload });
export const setDestinationLoading = (payload?: any) => ({ type: FlightActionTypes.SET_DESTINATION_LOADING, payload });
export const getAll = (payload: any) => ({ type: FlightActionTypes.GET_ALL, payload });
export const setAll = (payload: any) => ({ type: FlightActionTypes.SET_ALL, payload });
export const setOriginPlaces = (payload: any) => ({ type: FlightActionTypes.SET_ORIGIN_PLACES, payload });
export const setDestinationPlaces = (payload: any) => ({ type: FlightActionTypes.SET_DESTINATION_PLACES, payload });
export const setCurrencies = (payload: any) => ({ type: FlightActionTypes.SET_CURRENCIES, payload });
export const setCountries = (payload: any) => ({ type: FlightActionTypes.SET_COUNTRIES, payload });
export const errorRequest = (payload: any) => ({ type: FlightActionTypes.ERROR_REQUEST, payload });