import React from 'react';
import { Switch } from 'react-router-dom';

import flight from './flight';
import result from './result';

import { FlightContextProvider } from '../contexts/flight';

export const routes = () => {
  return (
    <FlightContextProvider>
      <Switch>
        {flight}
        {result}
      </Switch>
    </FlightContextProvider>
  );
};
