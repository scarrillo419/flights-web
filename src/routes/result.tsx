import React from 'react';
import reactLoadable from 'react-loadable';

import BlankLayout from '../containers/layout/blank';

const routes = [
  <BlankLayout
    key="results-admin"
    path="/results"
    component={reactLoadable({
      loader: () => import('../containers/results/Results'),
      loading: () => <div>Loading...</div>,
    })}
  />
];

export default routes;