import React from 'react';
import reactLoadable from 'react-loadable';

import BlankLayout from '../containers/layout/blank';

const routes = [
  <BlankLayout
    key="flights-admin"
    path="/flights"
    component={reactLoadable({
      loader: () => import('../containers/flights/Flights'),
      loading: () => <div>Loading...</div>,
    })}
  />
];

export default routes;