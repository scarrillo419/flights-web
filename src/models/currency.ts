export class Currency {
  public Code: string;
  public DecimalDigits: number;
  public DecimalSeparator: string;
  public RoundingCoefficient: number;
  public SpaceBetweenAmountAndSymbol: boolean;
  public Symbol: string;
  public SymbolOnLeft: boolean;
  public ThousandsSeparator: string;

  constructor(data: any) {
    this.Code = data.Code;
    this.DecimalDigits = data.DecimalDigits;
    this.DecimalSeparator = data.DecimalSeparator;
    this.RoundingCoefficient = data.RoundingCoefficient;
    this.SpaceBetweenAmountAndSymbol = data.SpaceBetweenAmountAndSymbol;
    this.Symbol = data.Symbol;
    this.SymbolOnLeft = data.SymbolOnLeft;
    this.ThousandsSeparator = data.ThousandsSeparator;
  }
}
