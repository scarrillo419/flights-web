import axios from 'axios';

const apiurl = process.env.REACT_APP_API_URL ?? '';

export default {
  async getFlights(payload: any) {
    const { data } = await axios.get(
      `${apiurl}/browsequotes/v1.0/${payload.country}/${payload.currency}/${payload.locale}/${payload.originplace}/${payload.destinationplace}/${payload.outboundpartialdate}/`,
      { params: { query: payload.query } },
    );
    return data;
  },
  async getPlaces(payload: any) {
    const { data } = await axios.get(`${apiurl}/autosuggest/v1.0/${payload.country}/${payload.currency}/${payload.locale}/`, { params: { query: payload.query } });
    return data;
  },
  async getCurrencies(payload: any) {
    const { data } = await axios.get(`${apiurl}/reference/v1.0/currencies`);
    return data;
  },
  async getCountries(payload: any) {
    const { data } = await axios.get(`${apiurl}/reference/v1.0/countries/en-US/`);
    return data;
  }
};
