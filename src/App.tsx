import React, { memo, useEffect } from 'react';
import { Router, Redirect } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import './App.css';

import { routes as Routes } from './routes';
import { setHeaders } from './utils';

const history = createBrowserHistory();

const App = memo(() => {
  useEffect(() => {
    setHeaders();
  }, []);

  return (
    <Router history={history}>
      <Redirect from="/" to="/flights" />
      <Routes />
    </Router>
  );
});

export default App;
